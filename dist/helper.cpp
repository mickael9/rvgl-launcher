////////////////////////////////////////////////////////////////////////////////
//
// RVGL Launcher (Generic) Copyright (c) The RV Team
//
// Desc: helper.cpp
// Windows setup module for RVGL Launcher.
//
// 2021-01-09 (Huki)
// File inception.
//
////////////////////////////////////////////////////////////////////////////////

#include <windows.h>
#include <shlobj.h>
#include <commctrl.h>
#include <stdio.h>

int CreateShortcut(const char *app_path, char *target, char *args, char *icon)
{
  char desktop_path[MAX_PATH];
  char link_file[MAX_PATH];
  char desc[MAX_PATH];

  printf("Creating shortcut file...\n");

  if (!app_path[0]) {
    printf("No app path detected (-path <app-path>).\n");
    return -1;
  }

  if (SHGetFolderPath(NULL, CSIDL_DESKTOPDIRECTORY, NULL, 0, desktop_path) != S_OK) {
    printf("Could not get Desktop folder.\n");
    return -1;
  }

  snprintf(link_file, MAX_PATH, "%s\\RVGL Launcher.lnk", desktop_path);
  snprintf(desc, MAX_PATH, "Launcher and package manager for RVGL");

  if (!target[0]) {
    snprintf(target, MAX_PATH, "%s\\rvgl_launcher.exe", app_path);
  }
  if (!icon[0]) {
    strcpy(icon, target);
  }

  HRESULT r = CoInitialize(NULL);
  if (r != S_OK) {
    printf("Could not initialize COM.\n");
    return -1;
  }

  IShellLink *psl;
  r = CoCreateInstance(CLSID_ShellLink, NULL, CLSCTX_INPROC_SERVER, IID_IShellLink, (LPVOID *)&psl);
  if (r != S_OK) {
    printf("Could not create an IShellLink instance.\n");
    CoUninitialize();
    return -1;
  }

  psl->SetPath(target);
  psl->SetArguments(args);
  psl->SetDescription(desc);
  psl->SetIconLocation(icon, 0);
  psl->SetWorkingDirectory(app_path);

  IPersistFile *ppf;
  r = psl->QueryInterface(IID_IPersistFile, (LPVOID *)&ppf);
  if (r != S_OK) {
    printf("Could not query an IPersistFile interface.\n");
    psl->Release();
    CoUninitialize();
    return -1;
  }

  WCHAR wsz[MAX_PATH];
  if (!MultiByteToWideChar(CP_ACP, 0, link_file, -1, wsz, MAX_PATH)) {
    printf("Could not convert path to wide char.\n");
    ppf->Release();
    psl->Release();
    CoUninitialize();
    return -1;
  }

  r = ppf->Save(wsz, TRUE);
  if (r != S_OK) {
    printf("Could not save shortcut file.\n");
    ppf->Release();
    psl->Release();
    CoUninitialize();
    return -1;
  }

  ppf->Release();
  psl->Release();
  CoUninitialize();
  return 0;
}

void ProcessCommandLine(const char *option, char *result)
{
  for (int i = 1; i < __argc; ++i) {
    if (!strcmp(__argv[i], option)) {
      if (i+1 < __argc && __argv[i+1] && __argv[i+1][0] != '-') {
        strncpy(result, __argv[++i], MAX_PATH - 1);
        printf("%s: %s\n", option, result);
      }
      break;
    }
  }
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
  char path[MAX_PATH] = {};
  char command[MAX_PATH] = {};
  char args[MAX_PATH] = {};
  char icon[MAX_PATH] = {};

  // process command lines
  ProcessCommandLine("-path", path);
  ProcessCommandLine("-command", command);
  ProcessCommandLine("-args", args);
  ProcessCommandLine("-icon", icon);

  for (int i = 1; i < __argc; ++i) {
    // create shortcut
    if (!strcmp(__argv[i], "-shortcut")) {
      return CreateShortcut(path, command, args, icon);
    }
  }

  return 0;
}
