#!/bin/bash

for i in "$@"; do
  case $i in
    --linux) linux=linux;;
    --macos) macos=macos;;
    --win32) win32=win32; gcc=i686-w64-mingw32-gcc;;
    --win64) win32=win64; gcc=x86_64-w64-mingw32-gcc;;
    --build) build=true;;
    --package) package=true;;
    --clean) clean=true;;
    --helper) helper=true;;
    --release) release=true;;
  esac
done

# Build helper.exe for Windows
if [ "$helper" = true ]; then
  if [ "$win32" ]; then
    ${gcc} -o ${win32}/helper.exe helper.cpp -lole32 -luuid -s
  fi
  exit
fi

# Clean build files
if [ "$clean" = true ]; then
  rm -f *.zip *.gz *.txt *.json
  rm -rf win32/build
  rm -rf win64/build
  rm -rf macos/build
  rm -rf macos/rvgl_launcher
  rm -rf macos/rvgl_launcher.app
  rm -f win32/rvgl_launcher.exe
  rm -f win64/rvgl_launcher.exe
  rm -f linux/rvgl_launcher
  rm -f win32/helper.exe
  rm -f win64/helper.exe
  exit
fi

# Build with PyInstaller
if [ "$build" = true ]; then
  python=python
  if [ "$PYTHONPATH" ]; then
    python="${PYTHONPATH}/python"
  fi

  if [ "$win32" ]; then
    python version.py # Always use native python
    "${python}.exe" -m PyInstaller rvgl_launcher_win32.spec --workpath ${win32}/build --distpath ${win32}
  fi

  if [ "$macos" ]; then
    "${python}" -m PyInstaller rvgl_launcher_macos.spec --workpath ${macos}/build --distpath ${macos}
  fi
fi

# Generate release packages
if [ "$package" = true ]; then
  if [ "$win32" ]; then
    rm -f rvgl_launcher_${win32}.zip
    (cd ${win32} && zip -r ../rvgl_launcher_${win32}.zip * -x "build*" "*.spec")
    (cd .. && zip -r dist/rvgl_launcher_${win32}.zip icons repos)
  fi

  if [ "$macos" ]; then
    rm -f rvgl_launcher_${macos}.zip
    (cd ${macos} && 7zz a ../rvgl_launcher_${macos}.zip -r "rvgl_launcher.app")
  fi

  if [ "$linux" ]; then
    rm -f rvgl_launcher_${linux}.zip
    files="icons repos rv_launcher appdirs.py rvgl_launcher.py"
    (cd .. && zip -r dist/rvgl_launcher_${linux}.zip ${files} -x "*__pycache__*")
  fi
fi

# Generate JSON spec for release
if [ "$release" = true ]; then
  python release.py # Always use native python
fi
