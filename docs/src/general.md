# General Information

<!-- toc -->

## The "rvmm://" URI

RVGL Launcher supports the custom URI scheme `rvmm://`. When a link starting
with this URI scheme is clicked, or typed manually in your browser, it will
automatically start the launcher and perform a command.

Before you can use this functionality, the URI must be registered. This is
automatically done the first time the launcher is run. If you want to register
it again for any reason, use the `Help -> Register URI` menu option.

The following commands are currently supported:

- `rvmm:///`: Run the launcher and do nothing else. If already running, switch
  the focus to the running launcher window.

- `rvmm://join/<ip-address>`: Launch the game and directly connect to the specified
  IP address. For example, `rvmm://join/vas.rv.gl` attempts to join a game hosted
  at `vas.rv.gl`.

- `rvmm://<download-link>`: Download content specified by the download link and
  install it to the local pack. Automatically downloading and installing content
  this way is limited to a few whitelisted domains:
  ```
  # Re-Volt World
  rvmm://www.revoltworld.net/dl/harvey/?ind=1624368413026&filename=harvey.zip&wpdmdl=28037&refresh=60d461eedf4181624531438

  # Re-Volt Zone
  rvmm://www.revoltzone.net/trackfiles/home_bat.zip
  rvmm://www.revoltzone.net/carfiles/fd_s30.zip

  # Re-Volt XTG
  rvmm://www.revoltxtg.co.uk/files/Xtr/R/P/pod_zed/pod_zed.zip

  # Re-Volt I/O
  rvmm://files.re-volt.io/packs/christmas/2020/grinch.7z

  # Duc's RV Site
  rvmm://hajduc.com/downloads/no_repo_tracks_200408.zip
  ```

## Automatic Updates

The launcher updates itself automatically. Updates are checked at each start. When
a new version of the launcher is available, it'll be downloaded automatically and
installed when you quit the application.

> Automatic updates are turned off if the launcher is run from a git repo. So
a way to disable checking for updates is to create an empty `.git` folder in the
launcher's folder.

## Settings and Files

The launcher keeps everything in its data and config folders. In a `standard`
install, both are tied to the current user account.

- On Linux, these folders are usually: `~/.local/share/rvmm` and `~/.config/rvmm`.
- On Windows, there is a single folder at: `C:\Users\<user-name>\AppData\Local\rvmm`.
- On macOS, there is a single folder at: `~/Library/Application Support/rvmm`.

The `custom` install type allows you to override the data folder location.

### Data Folder

Can be opened with `File -> Open Data Folder`. This folder contains:

- *downloads*: Downloaded packages.
- *packs*: Installed packages.
- *save*: Save games and replays.
- *update*: Staging area for updates.

### Config Folder

Can be opened with `File -> Open Config Folder`. This folder contains:

- *logs*: Application logs.
- *recipes*: Game presets.
- *repos*: Downloaded repository files.
- *config.json*: Configuration file.

> Use `File -> Clean` to clear downloads and logs.

## Show Console

Console messages can be viewed by enabling `Help -> Show Console`. This adds
a Console tab to the UI, where log messages and errors are printed.

[![Console tab](screens/console.th.png)](screens/console.png)
