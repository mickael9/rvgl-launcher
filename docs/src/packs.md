# Packages

<!-- toc -->

[![Packs tab](screens/packs.th.png)](screens/packs.png)

## Adding Packages

RVGL Launcher utilizes a unique package management system to install and manage your
game data. Content is stored in the form of packs, in separate folders that can be 
enabled or disabled on their own. The system should feel familiar to users of
software like [Mod Organizer](https://github.com/ModOrganizer2/modorganizer).

The `Packs` tab lists all available packages.  Packages are sourced from Re-Volt I/O
and other third-party repositories. You can see the list of available repositories
and add more from the [Repositories tab](./repos.md).

The initial setup installs and enables those packs defined by the chosen
preset, or the packs you selected in case of the `Custom` preset.

Additional packs can be enabled simply by enabling the checkboxes in the `Packs`
tab. If any of the enabled packages are not installed yet, they'll be marked for
install. You can install them with the `Install Packages` button.

> Double click an installed pack to open the pack's folder.

## Package States

Packages can be in one of these states:

- **Installed**: The package is installed and up-to-date.
- **Update Available**: The package has a new version available.
- **Pending Install**: The package is enabled but not installed yet.
- **Not Installed**: The package is not installed.

> Use `Update Pack Info` to refresh the package information. It's automatically
refreshed once every 15 minutes.

## Managing Presets

By default, enabling or disabling packages causes your selection to be saved in
the `default` preset. The `New Preset` button allows you to create a new preset.
This will prompt you to select a base preset and type the preset name.

You can then enable or disable packages as usual and your selection is saved in
your new preset. To remove a preset, select the preset and press `Delete Preset`.
Note that you cannot delete the default preset.

> Packages disabled by all presets can be safely removed. If they were installed
previously, they still remain in the filesystem. To clean them up, use the
`File -> Clean` option.

## Local Packages

Local content is content that is not part of a repository. This could be content
you downloaded or content created by yourself. You can add your own content
packs by following these steps:

- Use `File -> Open Data Folder` to open the folder containing game data. Content
    in the root folder belong to the `default` pack. Additional packages are
    installed inside the `packs` sub-folder. 
- Make a new folder inside `packs` (say, *mycontent*) and place your content
    in there. These folders will be recognized by the launcher as local content
    packs. They can be enabled or disabled like any other pack.
- Use `Update Pack Info` to refresh the packages list. It should now display your
    newly added pack. Use the checkbox beside the pack to enable or disable it.

> Edits made through the in-game editor go into a special pack folder called
`local`. If you're creating your own content, you must place the content in
`packs/local` and make sure it's enabled in your preset.

