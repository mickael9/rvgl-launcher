# RVGL Launcher

RVGL Launcher is a cross-platform installer, launcher and package manager for [RVGL](https://rvgl.org).
It's currently available on Windows and GNU/Linux for both 32-bit and 64-bit architectures.

[![Install tab](screens/install.th.png)](screens/install.png)

[ [Game tab](screens/launch.png) |
[Events tab](screens/events.png) |
[Packs tab](screens/packs.png) |
[Repositories tab](screens/repos.png) |
[Local tab](screens/local.png) |
[Console tab](screens/console.png) ]

## Features

- Install game and content packs and keep them up-to-date.
- View list of online events and join them directly.
- Enable or disable content packs on the fly.
- Add your own content and repositories.

## Download

Prepackaged builds are available below:

**[↓]** [**RVGL Launcher for Windows (32-bit)**](https://rvgl.org/downloads/rvgl_launcher_win32.zip)

**[↓]** [**RVGL Launcher for Windows (64-bit)**](https://rvgl.org/downloads/rvgl_launcher_win64.zip)

**[↓]** [**RVGL Launcher for GNU/Linux**](https://rvgl.org/downloads/rvgl_launcher_linux.zip)

**[↓]** [**RVGL Launcher for macOS**](https://rvgl.org/downloads/rvgl_launcher_macos.zip)

## Installation

Extract the zip file to a new folder, preferably in your Documents folder or some place
where you have write access. Then depending on your platform, run `rvgl_launcher.exe` or
`rvgl_launcher.py`. On macOS, you could simply drag and drop the `RVGL Launcher.app` folder
to Applications or Desktop and run it.

## Requirements

- An internet connection is required.
- The Windows builds require Windows 7 or above.
- The macOS app requires an Intel or M1 device running macOS 10.13+.

### Linux

Make sure you get the following dependencies from your package manager.

- Python 3.8+
- Python Modules:
  - wxPython
  - requests
  - packaging
- 7-Zip
- xdg-utils

#### Ubuntu

```
sudo apt install python3-wxgtk4.0 python3-requests python3-packaging p7zip-full xdg-utils
```

#### Arch Linux

```
sudo pacman -S python-wxpython python-requests python-packaging p7zip xdg-utils
```

